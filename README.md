## React Starter Express

### Purpose

This project is a simple setup of using React with Express.  This configuration is based off of the React.js documentation on [Getting Started](https://facebook.github.io/react/docs/getting-started.html). The build process is simple and uses gulp to transpile jsx into a bundle.js file to be included with both the react, react-dom, and jsx components.

This project uses Express to serve static files.

### How To use

To watch and transpile files into a bundle:

`npm run dev`

To run simple static server:

`npm run serve`

Dev server currently uses nodemon to run express.

### Things to Adjust

* Add ability to watch server to restart/use livereload
