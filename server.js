// app.js

var express = require('express');
var request = require('request');
var queryString = require('querystring');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var session = require('express-session');
var logger = require('morgan');

//setup
var app = express();
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(logger('dev'));
app.use(cookieParser());
app.use(session({
  resave: false,
  saveUninitialized: true,
  secret: 'coopertosteeltoahingethatisfaltered'
}));

//serving static
app.use(express.static(__dirname + '/public'));
app.use('/build', express.static(__dirname + '/build'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));

function errorHandler(err, req, res, next) {
  if(res.headersSent) {
    return next(err);
  }
  res.status(500);
  res.render('error', { error: err });
}

app.use(errorHandler);

//server starter
var server = app.listen(4000, function() {
  console.log('Server is running on localhost:4000');
});
